/*
** channels.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu May 12 13:32:55 2016 Karmes Lionel
** Last update Sun May 15 21:24:10 2016 Karmes Lionel
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "channels.h"

int	load_ip_port(t_data_chan *data, const struct sockaddr_in *addr)
{
  char	ip[56];
  char	*nb;
  char	*tmp;
  int	i;
  int	port;

  port = ntohs(addr->sin_port);
  data->port[0] = port / 256;
  data->port[1] = port % 256;
  strcpy(ip, inet_ntoa(addr->sin_addr));
  i = 0;
  tmp = ip;
  while (i < 3)
    {
      nb = tmp;
      if (!(tmp = strchr(nb, '.')))
	return (0);
      *tmp = '\0';
      data->ip[i] = atoi(nb);
      ++tmp;
      ++i;
    }
  nb = tmp;
  data->ip[i] = atoi(nb);
  return (1);
}

int			build_data_chan_pasv(t_data_chan *data)
{
  struct sockaddr_in	addr;
  socklen_t		addrlen;
  struct protoent	*pe;

  pe = getprotobyname("TCP");
  if ((data->sock = socket(AF_INET, SOCK_STREAM, pe->p_proto)) == -1)
    return (fprintf(stderr, "err: data channel socket(AF_INET, ...) %s\n",
                    strerror(errno)), -1);
  addr.sin_family = AF_INET;
  addr.sin_port = 0;
  addr.sin_addr.s_addr = INADDR_ANY;
  addrlen = sizeof(struct sockaddr_in);
  if (bind(data->sock, (struct sockaddr*)&addr, addrlen)
      == -1)
    return (fprintf(stderr, "err: data channel bind(...) %s\n",
		    strerror(errno)), close(data->sock), -1);
  if (listen(data->sock, 1) == -1)
    return (fprintf(stderr, "err: data channel listen(...) %s \n",
		    strerror(errno)), close(data->sock), -1);
  if (getsockname(data->sock, (struct sockaddr*)&addr, &addrlen) == -1)
    return (fprintf(stderr, "err: data channel getsockname(...) %s \n",
		    strerror(errno)), close(data->sock), -1);
  if (!load_ip_port(data, &addr))
    return (close(data->sock), 0);
  return (1);
}

static int	build_port(t_data_chan* data, char* port)
{
  char		*nb;

  nb = port;
  if (!(port = strpbrk(port, ",.")))
    return (0);
  *port = '\0';
  data->port[0] = atoi(nb);
  ++port;
  nb = port;
  data->port[1] = atoi(nb);
  return (1);
}

int	build_data_chan_port(t_data_chan* data, const char *str)
{
  int	i;
  char	*nb;
  char	*tmp;
  char	*c;

  if (!(tmp = strdup(str)))
    return (-1);
  c = tmp;
  i = 0;
  while (i < 4)
    {
      nb = tmp;
      if (!(tmp = strpbrk(tmp, ",.")))
	return (free(tmp), 0);
      *tmp = '\0';
      data->ip[i] = atoi(nb);
      ++tmp;
      ++i;
    }
  if (!build_port(data, tmp))
    return (free(c), 0);
  free(c);
  return (1);
}
