/*
** checkArg.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed May  4 16:21:12 2016 Karmes Lionel
** Last update Sun May 15 21:17:50 2016 Karmes Lionel
*/

#include <unistd.h>
#include <stdio.h>
#include "checkArgs.h"

int	check_args(int port, const t_path *path)
{
  if (port <= 0 || !path->real)
    return (printf("Usage : ./server port>0path\n"), 0);
  if (access(path->real, F_OK) == -1)
    return (printf("err: path '%s' does'nt exist\n", path->real), 0);
  return (1);
}
