/*
** cmd_data.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Fri May 13 14:46:55 2016 Karmes Lionel
** Last update Sun May 15 21:29:20 2016 Karmes Lionel
*/

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "cmd_X.h"

static int	cmd_Stor_next(const char* filename, int data_fd, t_path* path)
{
  int		fd;
  char		buff[1024];
  int		c;
  char		tmp[PATH_MAX];

  if (filename[0] == '/')
    strcpy(tmp, path->root_real);
  else
    strcpy(tmp, path->real);
  strcat(tmp, filename);
  if ((fd = creat(tmp, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH))
      == -1)
    return (0);
  while ((c = read(data_fd, buff, 1024)) > 0)
    if (write(fd, buff, c) == -1)
      return (fprintf(stderr, "Err: write(...) %s\n", strerror(errno)), -1);
  close(fd);
  return (1);
}

static int	cmd_Stor_or_Retr(const t_cmd *cmd, t_channels *chan,
				 t_path *path)
{
  int		ret;
  char		*tmp;
  char		*c;

  ret = 0;
  if (!(tmp = strdup(cmd->param)))
    return (-1);
  if ((c = strrchr(tmp, '/')))
    *(c + 1) = '\0';
  else
    strcpy(tmp, ".");
  if (!cmd->param || cmd->param[0] == '\0')
    dprintf(chan->clt_fd, "501 Syntax error in parameters or arguments.\r\n");
  else if (!strcasecmp(cmd->cmd, "STOR"))
    ret = valid_dir(path, tmp, 0);
  else
    ret = valid_path(path, cmd->param);
  if (ret == 0)
    dprintf(chan->clt_fd, "550 Requested action not taken.\n"
	    "File unavailable (e.g., file not found, no access).\r\n");
  free(tmp);
  return (ret);
}

int	cmd_Stor(const t_cmd *cmd, t_channels *chan, t_path *path)
{
  int	ret;
  int	data_fd;

  (void)path;
  if (strcasecmp(cmd->cmd, "STOR"))
    return (0);
  if ((ret = cmd_Stor_or_Retr(cmd, chan, path)) == -1)
    return (-1);
  else if (ret > 0)
    if ((data_fd = link_data_channel(chan)) > -1)
      {
	if ((ret = cmd_Stor_next(cmd->param, data_fd, path)) == -1)
	  return (-1);
	else if (ret == 0)
	  dprintf(chan->clt_fd, "550 Requested action not taken."
		  "File unavailable (e.g., file not found, no access.\r\n");
	else
	  dprintf(chan->clt_fd, "226 Closing data connection."
		  "Requested file action successful (for example, file"
		  "transfer or file abort).\r\n");
	close_data_chan(&chan->data, data_fd);
      }
  return (1);
}

static int	cmd_Retr_next(const char* filename, int data_fd, t_path* path)
{
  int		fd;
  char		buff[1024];
  int		c;
  char		tmp[PATH_MAX];

  if (filename[0] == '/')
    strcpy(tmp, path->root_real);
  else
    strcpy(tmp, path->real);
  strcat(tmp, filename);
  if ((fd = open(tmp, O_RDONLY)) == -1)
    return (0);
  while ((c = read(fd, buff, 1024)) > 0)
    if (write(data_fd, buff, c) == -1)
      return (fprintf(stderr, "Err: write(...) %s\n", strerror(errno)), -1);
  close(fd);
  return (1);
}

int	cmd_Retr(const t_cmd *cmd, t_channels *chan, t_path *path)
{
  int	ret;
  int	data_fd;

  (void)path;
  if (strcasecmp(cmd->cmd, "RETR"))
    return (0);
  if ((ret = cmd_Stor_or_Retr(cmd, chan, path)) == -1)
    return (-1);
  else if (ret > 0)
    if ((data_fd = link_data_channel(chan)) > -1)
      {
	if ((ret = cmd_Retr_next(cmd->param, data_fd, path)) == -1)
	  return (-1);
	else if (ret == 0)
	  dprintf(chan->clt_fd, "550 Requested action not taken."
		  "File unavailable (e.g., file not found, no access.\r\n");
	else
	  dprintf(chan->clt_fd, "226 Closing data connection."
		  "Requested file action successful (for example, file"
		  "transfer or file abort).\r\n");
	close_data_chan(&chan->data, data_fd);
      }
  return (1);
}
