/*
** channels.h for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu May 12 12:54:42 2016 Karmes Lionel
** Last update Sun May 15 21:34:11 2016 Karmes Lionel
*/

#ifndef CHANNELS_H_
# define CHANNELS_H_

# include <netinet/in.h>
# include <sys/types.h>
# include <sys/socket.h>
# include "parseCmd.h"
# include "path.h"

enum	transfert_data
  {
    NONE,
    PASSIV,
    ACTIV
  };

typedef struct	s_data_chan
{
  int		sock;
  unsigned char	ip[4];
  unsigned char	port[2];
  int		mode;
}		t_data_chan;

typedef struct	s_channels
{
  int		clt_fd;
  t_data_chan	data;
}		t_channels;

int		load_ip_port(t_data_chan*, const struct sockaddr_in*);
int		build_data_chan_pasv(t_data_chan*);
int		build_data_chan_port(t_data_chan*, const char*);
int		accept_clt(const t_data_chan*);
int		connect_clt(t_data_chan*);
int		link_data_channel(t_channels*);
void		close_data_chan(t_data_chan*, int);

#endif /* !CHANNELS_H_ */
