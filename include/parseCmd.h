/*
** parseCmd.h for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu May  5 11:43:43 2016 Karmes Lionel
** Last update Thu May  5 12:10:30 2016 Karmes Lionel
*/

#ifndef PARSE_CMD_H_
# define PARSE_CMD_H_

typedef	struct	s_cmd
{
  char*		cmd;
  char*		param;

}		t_cmd;

t_cmd		*parse_cmd(char *cmd);
void		free_cmd(t_cmd *);
void		print_cmd(const t_cmd *);

#endif /* !PARSE_CMD_H_ */
