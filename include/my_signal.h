/*
** my_signal.h for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_my_signal/include
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed May  4 16:32:08 2016 Karmes Lionel
** Last update Sun May 15 15:08:38 2016 Karmes Lionel
*/

#ifndef MY_SIGNAL_H_
# define MY_SIGNAL_H_

void	sig_child(int);

#endif /* !MY_SIGNAL_H_ */
