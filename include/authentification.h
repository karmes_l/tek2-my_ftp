/*
** authentification.h for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu May  5 12:05:00 2016 Karmes Lionel
** Last update Sun May 15 21:15:56 2016 Karmes Lionel
*/

#ifndef AUTHENTIFICATION_H_
# define AUTHENTIFICATION_H_

# include "readClient.h"
# include "parseCmd.h"
# include "channels.h"

int		authentification(t_channels*);

#endif /* !AUTHENTIFICATION_H_ */
