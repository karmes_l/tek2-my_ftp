/*
** path.h for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Sun May  8 15:18:57 2016 Karmes Lionel
** Last update Sun May 15 21:34:10 2016 Karmes Lionel
*/

#ifndef PATH_H_
# define PATH_H_

# include "linux/limits.h"

typedef struct	s_path
{
  char		real[PATH_MAX];
  char		root_real[PATH_MAX];
  char*		rela;
}		t_path;

int   		init_path(t_path *, const char *);
int		valid_dir(const t_path *, const char *, int);
int		valid_path(const t_path *, const char *);

#endif /* !PATH_H_ */
