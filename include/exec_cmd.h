/*
** exec_cmd.h for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp/include
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu May  5 19:24:40 2016 Karmes Lionel
** Last update Thu May 12 17:09:58 2016 Karmes Lionel
*/

#ifndef EXEC_CMD_H_
# define EXEC_CMD_H_

# include "path.h"
# include "cmd_X.h"
# include "channels.h"

int	exec_cmd(const t_cmd*, t_channels*, t_path*);

#endif /* !EXEC_CMD_H_ */
