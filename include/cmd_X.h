/*
** cmd_X.h for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu May  5 12:05:00 2016 Karmes Lionel
** Last update Sun May 15 21:48:07 2016 Karmes Lionel
*/

#ifndef CMD_X_H_
# define CMD_X_H_

# include "path.h"
# include "parseCmd.h"
# include "readClient.h"
# include "channels.h"

int		cmd_Help(const t_cmd *, t_channels*, t_path*);
int		cmd_Noop(const t_cmd *, t_channels*, t_path*);
int		cmd_Quit(const t_cmd *, t_channels*, t_path*);
int		cmd_User(const t_cmd *, t_channels*, t_path*);
int		cmd_Pass(const t_cmd *, t_channels*, t_path*);
int		cmd_Pwd(const t_cmd *, t_channels*, t_path*);
int		cmd_Cwd(const t_cmd *, t_channels*, t_path*);
int		cmd_Cdup(const t_cmd *, t_channels*, t_path*);
int		cmd_Dele(const t_cmd *, t_channels*, t_path*);
int		cmd_Pasv(const t_cmd *, t_channels*, t_path*);
int		cmd_Port(const t_cmd *, t_channels*, t_path*);
int		cmd_List(const t_cmd *, t_channels*, t_path*);
int		cmd_Stor(const t_cmd *, t_channels*, t_path*);
int		cmd_Retr(const t_cmd *, t_channels*, t_path*);

#endif /* !CMD_X_H_ */
