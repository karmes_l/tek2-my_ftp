/*
** checkArgs.h for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp/include
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed May  4 16:29:49 2016 Karmes Lionel
** Last update Sun May  8 15:22:11 2016 Karmes Lionel
*/

#ifndef CHECK_ARGS_H_
# define CHECK_ARGS_H_

# include "path.h"

int	check_args(int port, const t_path *path);

#endif /* !CHECK_ARGS_H_ */
