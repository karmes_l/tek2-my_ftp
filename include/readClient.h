/*
** readClient.h for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp/include
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu May  5 19:24:45 2016 Karmes Lionel
** Last update Thu May 12 17:06:04 2016 Karmes Lionel
*/

#ifndef READ_CLIENT_H_
# define READ_CLIENT_H_

# define QUIT		2

# include "path.h"
# include "parseCmd.h"
# include "exec_cmd.h"
# include "channels.h"

char	*read_client(int);
int	handle_client_cmd(t_channels*, t_path*);

#endif /* !READ_CLIENT_H_ */
