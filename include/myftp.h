/*
** myftp.h for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp/include
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed May  4 16:32:08 2016 Karmes Lionel
** Last update Sun May 15 15:09:03 2016 Karmes Lionel
*/

#ifndef MYFTP_H_
# define MYFTP_H_

# include "channels.h"
# include "path.h"
# include "checkArgs.h"
# include "authentification.h"
# include "my_signal.h"

#endif /* !MYFTP_H_ */
