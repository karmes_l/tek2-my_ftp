/*
** cmd_X.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu May  5 18:31:26 2016 Karmes Lionel
** Last update Sun May 15 21:51:06 2016 Karmes Lionel
*/

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <linux/limits.h>
#include <unistd.h>
#include "cmd_X.h"

int	cmd_Help(const t_cmd *cmd, t_channels *chan, t_path *path)
{
  (void)path;
  if (strcasecmp(cmd->cmd, "HELP"))
    return (0);
  dprintf(chan->clt_fd, "214 The following commands are recognized.\n"
	  "CDUP CWD DELE HELP LIST NOOP PASS PASV PORT PWD QUIT RETR STOR"
	  "USER\r\n");
  return (1);
}

int	cmd_Noop(const t_cmd *cmd, t_channels *chan, t_path *path)
{
  (void)path;
  if (strcasecmp(cmd->cmd, "NOOP"))
    return (0);
  dprintf(chan->clt_fd, "200 Command okay.\r\n");
  return (1);
}

int	cmd_Quit(const t_cmd *cmd, t_channels *chan, t_path *path)
{
  (void)path;
  if (strcasecmp(cmd->cmd, "QUIT"))
    return (0);
  dprintf(chan->clt_fd, "221 Service closing control connection.\n"
	  "Logged out if appropriate.\r\n");
  return (QUIT);
}

int	cmd_User(const t_cmd *cmd, t_channels *chan, t_path *path)
{
  (void)path;
  if (strcasecmp(cmd->cmd, "USER"))
    return (0);
  dprintf(chan->clt_fd, "530 Can't change from guest user.\r\n");
  return (1);
}

int	cmd_Pass(const t_cmd *cmd, t_channels *chan, t_path *path)
{
  (void)path;
  if (strcasecmp(cmd->cmd, "PASS"))
    return (0);
  dprintf(chan->clt_fd, "230 User logged in, proceed.\r\n");
  return (1);
}
