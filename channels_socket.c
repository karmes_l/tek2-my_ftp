/*
** channels_socket.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Fri May 13 15:08:57 2016 Karmes Lionel
** Last update Sun May 15 21:24:51 2016 Karmes Lionel
*/

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "channels.h"

int			accept_clt(const t_data_chan* data)
{
  struct sockaddr_in	addr;
  int			clt_data;
  socklen_t		addrlen;

  addrlen = sizeof(struct sockaddr_in);
  if ((clt_data = accept(data->sock, (struct sockaddr*)&addr, &addrlen)) == -1)
    return (fprintf(stderr, "Err: accept(clt_data, ...) %s\n",
		    strerror(errno)), -1);
  return (clt_data);
}

static int	build_ip(struct sockaddr_in* addr, unsigned char *nb)
{
  char		ip[12 + 3 + 1];

  snprintf(ip, 56, "%hd.%hd.%hd.%hd", nb[0], nb[1], nb[2], nb[3]);
  inet_pton(AF_INET, ip, &(addr->sin_addr));
  return (1);
}

int			connect_clt(t_data_chan* data)
{
  int			clt_data;
  struct sockaddr_in	addr;
  struct protoent	*pe;

  pe = getprotobyname("TCP");
  if ((clt_data = socket(AF_INET, SOCK_STREAM, pe->p_proto)) == -1)
    return (fprintf(stderr, "err: socket(clt_data, ...) %s\n",
                    strerror(errno)), -1);
  data->sock = clt_data;
  addr.sin_family = AF_INET;
  addr.sin_port = htons(data->port[0] * 256 + data->port[1]);
  build_ip(&addr, data->ip);
  if (connect(clt_data, (struct sockaddr*)&addr, sizeof(struct sockaddr_in))
      == -1)
    return (fprintf(stderr, "err: connect(clt_data, ...) %s\n",
                    strerror(errno)), -1);
  return (1);
}

int	link_data_channel(t_channels* chan)
{
  int	data_fd;

  if (chan->data.mode == NONE)
    {
      dprintf(chan->clt_fd, "425 Use PASV or PORT before.\r\n");
      return (-1);
    }
  dprintf(chan->clt_fd, "150 File status okay;"
	  "about to open data connection.\r\n");
  if (chan->data.mode == PASSIV && (data_fd = accept_clt(&chan->data)) == -1)
    {
      dprintf(chan->clt_fd, "425 Can't open data connection.\r\n");
      return (-1);
    }
  else if (chan->data.mode == ACTIV)
    {
      if (connect_clt(&chan->data) == -1)
	{
	  dprintf(chan->clt_fd, "425 Can't open data connection.\r\n");
	  return (-1);
	}
      data_fd = chan->data.sock;
    }
  return (data_fd);
}

void	close_data_chan(t_data_chan* data, int clt_data)
{
  close(data->sock);
  if (data->mode == PASSIV)
    close(clt_data);
  data->sock = -1;
  data->mode = NONE;
}
