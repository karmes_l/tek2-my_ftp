/*
** cmd_list.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Sun May 15 21:11:25 2016 Karmes Lionel
** Last update Sun May 15 21:20:40 2016 Karmes Lionel
*/

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include "cmd_X.h"

static int      cmd_List_next(int data_fd)
{
  int		stdout;

  if ((stdout = dup(1)) == -1)
    return (fprintf(stderr, "Err: dup(1) %s\n", strerror(errno)), -1);
  if (dup2(data_fd, 1) == -1)
    return (fprintf(stderr, "Err: dup2(stdout, 1) %s\n", strerror(errno)), -1);
  system("ls -la");
  if (dup2(stdout, 1) == -1)
    return (fprintf(stderr, "Err: dup2(stdout, 1) %s\n", strerror(errno)), -1);
  return (1);
}

int	cmd_List(const t_cmd *cmd, t_channels *chan, t_path *path)
{
  int	ret;
  int	data_fd;

  (void)path;
  if (strcasecmp(cmd->cmd, "LIST"))
    return (0);
  if (cmd->param)
    dprintf(chan->clt_fd, "501 Syntax error in parameters or arguments.\r\n");
  else
    {
      if ((data_fd = link_data_channel(chan)) > -1)
	{
	  if ((ret = cmd_List_next(data_fd)) > -1)
	    dprintf(chan->clt_fd, "226 Closing data connection."
		    "Requested file action successful (for example, file"
		    "transfer or file abort).\r\n");
	  close_data_chan(&chan->data, data_fd);
	  return (ret);
	}
    }
  return (1);
}
