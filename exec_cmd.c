/*
** exec_cmd.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu May  5 19:11:07 2016 Karmes Lionel
** Last update Sun May 15 21:50:21 2016 Karmes Lionel
*/

#include <stdio.h>
#include "exec_cmd.h"

static void	init_tab(int (*tab[])(const t_cmd*, t_channels*, t_path*))
{
  tab[0] = &cmd_Help;
  tab[1] = &cmd_Noop;
  tab[2] = &cmd_Quit;
  tab[3] = &cmd_Pwd;
  tab[4] = &cmd_Cwd;
  tab[5] = &cmd_Cdup;
  tab[6] = &cmd_Dele;
  tab[7] = &cmd_Pasv;
  tab[8] = &cmd_Port;
  tab[9] = &cmd_List;
  tab[10] = &cmd_Stor;
  tab[11] = &cmd_Retr;
  tab[12] = &cmd_User;
  tab[13] = &cmd_Pass;
}

int	exec_cmd(const t_cmd* cmd, t_channels *chan, t_path *path)
{
  int	(*tab[14])(const t_cmd*, t_channels*, t_path*);
  int	i;
  int	ret;

  init_tab(tab);
  i = 0;
  while (i < 14 && !(ret = tab[i](cmd, chan, path)))
    ++i;
  if (i == 14)
    dprintf(chan->clt_fd, "500 Syntax error, command unrecognized.\n"
	    "This may include errors such as command line too long.\r\n");
  return (ret);
}
