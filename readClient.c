/*
** readFromClient.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed May  4 22:44:44 2016 Karmes Lionel
** Last update Sun May 15 21:39:19 2016 Karmes Lionel
*/

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include "readClient.h"

char		*read_client(int client_fd)
{
  static char	msg[1024];
  char		*end;

  bzero(msg, 1024);
  while (strlen(msg) < 1024 && !strstr(msg, "\r\n"))
    {
      end = strchr(msg, '\0');
      if (read(client_fd, end, sizeof(char) * (1024 - strlen(msg))) == -1)
	return (fprintf(stderr, "err: read(...) %s\n", strerror(errno)), NULL);
      if (msg[0] == '\0')
	return (msg);
    }
  return (msg);
}

int	handle_client_cmd(t_channels *chan, t_path *path)
{
  char*	rep;
  t_cmd	*cmd;
  int	quit;

  quit = 0;
  while (quit != QUIT)
    {
      if (!(rep = read_client(chan->clt_fd)) || !(cmd = parse_cmd(rep)))
	return (-1);
      if (rep[0] == '\0')
	quit = QUIT;
      else if (!cmd->cmd)
        dprintf(chan->clt_fd, "500 Syntax error, command unrecognized.\n"
                "This may include errors such as command line too long.\r\n");
      else if ((quit = exec_cmd(cmd, chan, path)) == -1)
	return (free_cmd(cmd), -1);
      free_cmd(cmd);
    }
  return (quit);
}
