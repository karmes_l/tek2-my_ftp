/*
** cmd_X.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu May  5 18:31:26 2016 Karmes Lionel
** Last update Sun May 15 21:22:54 2016 Karmes Lionel
*/

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <linux/limits.h>
#include <unistd.h>
#include "cmd_X.h"

int	cmd_Pwd(const t_cmd *cmd, t_channels *chan, t_path *path)
{
  if (strcasecmp(cmd->cmd, "PWD"))
    return (0);
  if (cmd->param)
    dprintf(chan->clt_fd, "501 Syntax error in parameters or arguments.\r\n");
  else
    dprintf(chan->clt_fd, "257 \"%s\"\r\n", path->rela);
  return (1);
}

int	cmd_Cwd(const t_cmd *cmd, t_channels *chan, t_path *path)
{
  int	ret;

  if (strcasecmp(cmd->cmd, "CWD"))
    return (0);
  if (!cmd->param || cmd->param[0] == '\0')
    dprintf(chan->clt_fd, "501 Syntax error in parameters or arguments.\r\n");
  else if ((ret = valid_dir(path, cmd->param, 1)) == -1)
    return (-1);
  else if (ret != 1)
    dprintf(chan->clt_fd, "550 Requested action not taken.\n"
	    "File unavailable (e.g., file not found, no access).\r\n");
  else
    {
      if (!getcwd(path->real, PATH_MAX))
	return (fprintf(stderr, "Err: getcwd(...) %s\n", strerror(errno)), -1);
      if (path->real[strlen(path->real)] != '/')
	strcat(path->real, "/");
      dprintf(chan->clt_fd, "250 Requested file action okay, completed.\r\n");
    }
  return (1);
}

int	cmd_Cdup(const t_cmd *cmd, t_channels *chan, t_path *path)
{
  if (strcasecmp(cmd->cmd, "CDUP"))
    return (0);
  if (cmd->param)
    dprintf(chan->clt_fd, "501 Syntax error in parameters or arguments.\r\n");
  else if (strrchr(path->rela, '/') != path->rela)
    {
      if (chdir("..") == -1)
	return (fprintf(stderr, "Err: chdir('..') %s\n", strerror(errno)), -1);
      if (!getcwd(path->real, PATH_MAX))
	return (fprintf(stderr, "Err: getcwd(...) %s\n", strerror(errno)), -1);
      if (path->real[strlen(path->real)] != '/')
	strcat(path->real, "/");
      dprintf(chan->clt_fd, "200 Command okay.\r\n");
    }
  else
    dprintf(chan->clt_fd, "550 Requested action not taken.\n"
	    "File unavailable (e.g., file not found, no access).\r\n");
  return (1);
}

static int	cmd_Dele_next(const t_cmd *cmd, int client_fd, t_path *path)
{
  char		tmp[PATH_MAX];

  if (cmd->param[0] == '/')
    strcpy(tmp, path->root_real);
  else
    strcpy(tmp, path->real);
  strcat(tmp, cmd->param);
  if (remove(tmp) == -1)
    dprintf(client_fd, "550 Requested action not taken.\n"
	    "File unavailable (e.g., file busy)\r\n");
  else
    dprintf(client_fd, "250 Requested file action okay, completed.\r\n");
  return (1);
}

int	cmd_Dele(const t_cmd *cmd, t_channels *chan, t_path *path)
{
  int	ret;

  if (strcasecmp(cmd->cmd, "DELE"))
    return (0);
  if (!cmd->param || cmd->param[0] == '\0')
    dprintf(chan->clt_fd, "501 Syntax error in parameters or arguments.\r\n");
  else if ((ret = valid_path(path, cmd->param)) == -1)
    return (-1);
  else if (ret != 1)
    dprintf(chan->clt_fd, "550 Requested action not taken.\n"
	    "File unavailable (e.g., file not found, no access).\r\n");
  else
    return (cmd_Dele_next(cmd, chan->clt_fd, path));
  return (1);
}
