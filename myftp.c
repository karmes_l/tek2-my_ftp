/*
** myftp.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed May  4 13:29:25 2016 Karmes Lionel
** Last update Sun May 15 21:29:19 2016 Karmes Lionel
*/

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include "myftp.h"

int			handle_client(int client_fd, t_path *path)
{
  int			ret;
  t_channels		chan;

  chan.clt_fd = client_fd;
  chan.data.mode = NONE;
  dprintf(client_fd, "220 Service ready for new user.\r\n");
  if ((ret = authentification(&chan)) == -1)
    return (0);
  else if (ret == QUIT)
    return (1);
  ret = handle_client_cmd(&chan, path);
  if (chan.data.mode != NONE && chan.data.sock != -1)
    close(chan.data.mode);
  if (ret == -1)
    return (-1);
  return (1);
}

int			start_server(int server_fd, t_path *path)
{
  int			client_fd;
  struct sockaddr_in	addr;
  socklen_t		addrlen;
  pid_t			child;

  addrlen = sizeof(struct sockaddr_in);
  while (1)
    {
      if (signal(SIGCHLD, sig_child) == SIG_ERR)
	fprintf(stderr, "Err: signal(SIGCHILD) %s\n", strerror(errno));
      if ((client_fd = accept(server_fd,
			      (struct sockaddr*)&addr, &addrlen)) == -1)
	return (fprintf(stderr, "err: accept(...) %s\n", strerror(errno)), 0);
      if ((child = fork()) == -1)
	return (fprintf(stderr, "err: fork() %s\n", strerror(errno)),
		close(client_fd), 0);
      if (child == 0)
	{
	  if (!handle_client(client_fd, path))
	    return (close(client_fd), 0);
	  return (close(client_fd), 1);
	}
      close(client_fd);
    }
  return (1);
}

int			creat_server(int port)
{
  int			server_fd;
  struct protoent	*pe;
  int			optval;
  struct sockaddr_in	addr;

  pe = getprotobyname("TCP");
  if ((server_fd = socket(AF_INET, SOCK_STREAM, pe->p_proto)) == -1)
    return (fprintf(stderr, "err: socket(AF_INET, ...) %s\n",
		    strerror(errno)), -1);
  optval = 1;
  if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR,
		 &optval, sizeof(int)) == -1)
    return (fprintf(stderr, "err: setsockopt(..., SO_REUSEADDR, ...) %s\n",
		    strerror(errno)), close(server_fd), -1);
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = INADDR_ANY;
  if (bind(server_fd, (struct sockaddr*)&addr,
	   (sizeof(struct sockaddr_in))) == -1)
    return (fprintf(stderr, "err: bind(...) %s\n", strerror(errno)),
	      close(server_fd), -1);
  if (listen(server_fd, 128) == -1)
    return (fprintf(stderr, "err: listen(...) %s \n", strerror(errno)),
	    close(server_fd), -1);
  return (server_fd);
}

int		main(int ac, char **av)
{
  int		port;
  t_path	path;
  int		server_fd;

  if (ac < 3)
    return (printf("Usage : %s port>0 path\n", av[0]), 1);
  port = atoi(av[1]);
  if (!init_path(&path, av[2]))
    return (1);
  if (!check_args(port, &path))
    return (1);
  if ((server_fd = creat_server(port)) == -1)
    return (1);
  if (!start_server(server_fd, &path))
    return (close(server_fd), 1);
  close(server_fd);
  return (0);
}
