/*
** parseCmd.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu May  5 11:42:13 2016 Karmes Lionel
** Last update Sun May 15 21:29:18 2016 Karmes Lionel
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parseCmd.h"

t_cmd*		parse_cmd(char *line)
{
  t_cmd*	cmd;
  char		*c;

  if (!(cmd = malloc(sizeof(t_cmd))))
    return (NULL);
  if (!(cmd->cmd = strdup(line)))
    return (NULL);
  cmd->param = NULL;
  if ((c = strstr(cmd->cmd, "\r\n")) == NULL)
    cmd->cmd = NULL;
  else
    {
      *c = '\0';
      if ((c = strchr(cmd->cmd, ' ')) == NULL)
	cmd->param = NULL;
      else
	{
	  *c = '\0';
	  cmd->param = c + 1;
	}
    }
  return (cmd);
}

void	free_cmd(t_cmd *cmd)
{
  if (cmd->cmd)
    free(cmd->cmd);
  free(cmd);
}

void	print_cmd(const t_cmd* cmd)
{
  if (!cmd)
    printf("cmd : null\n");
  else
    printf("cmd '%s', param '%s'\n", cmd->cmd, cmd->param);
}
