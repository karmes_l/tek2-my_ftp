/*
** authentification.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu May  5 11:38:36 2016 Karmes Lionel
** Last update Sun May 15 21:49:49 2016 Karmes Lionel
*/

#include <stdio.h>
#include <string.h>
#include "authentification.h"

static int	cmd_User_auth(const char *param, int client_fd)
{
  if (!param)
    {
      dprintf(client_fd, "501 Syntax error in parameters or arguments.\r\n");
      return (0);
    }
  dprintf(client_fd, "331 User name okay, need password.\r\n");
  if (!strcasecmp(param, "Anonymous"))
    return (2);
  return (1);
}

static int	cmd_Pass_auth(const char *param, int user, int client_fd)
{
  if (user == 0)
    {
      dprintf(client_fd, "503 Login with USER first.\r\n");
      return (0);
    }
  else if (user == 1 || (param && param[0] != '\0'))
    {
      dprintf(client_fd, "530 Login incorrect\r\n");
      return (0);
    }
  dprintf(client_fd, "230 Login successful.\r\n");
  return (1);
}

int	login(int *user, const t_cmd* cmd, int client_fd)
{
  if (!strcasecmp(cmd->cmd, "USER"))
    *user = cmd_User_auth(cmd->param, client_fd);
  else if (!strcasecmp(cmd->cmd, "PASS"))
    {
      if ((*user = cmd_Pass_auth(cmd->param, *user, client_fd)) == 1)
	return (1);
    }
  else
    dprintf(client_fd, "530 Please login with USER and PASS\r\n");
  return (0);
}

int	authentification(t_channels *chan)
{
  char	*rep;
  t_cmd	*cmd;
  int	user;
  int	quit;

  user = 0;
  quit = 0;
  while (!quit)
    {
      if (!(rep = read_client(chan->clt_fd)) || !(cmd = parse_cmd(rep)))
	return (-1);
      if (rep[0] == '\0')
	quit = QUIT;
      else if (!cmd->cmd)
	dprintf(chan->clt_fd, "500 Syntax error, command unrecognized.\n"
		"This may include errors such as command line too long.\r\n");
      else if (!(quit = cmd_Quit(cmd, chan, NULL)))
	quit = login(&user, cmd, chan->clt_fd);
      free_cmd(cmd);
    }
  return (quit);
}
