/*
** cmd_data.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu May 12 12:55:31 2016 Karmes Lionel
** Last update Sun May 15 21:18:02 2016 Karmes Lionel
*/

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include "cmd_X.h"

static void	print_pasv(int fd, const t_data_chan *data)
{
  dprintf(fd, "227 Entering Passive Mode "
	  "(%hd,%hd,%hd,%hd,%hd,%hd).\r\n",
	  data->ip[0], data->ip[1], data->ip[2],
	  data->ip[3], data->port[0], data->port[1]);
}

int	cmd_Pasv(const t_cmd *cmd, t_channels *chan, t_path *path)
{
  int	ret;

  (void)path;
  if (strcasecmp(cmd->cmd, "PASV"))
    return (0);
  if (cmd->param)
    dprintf(chan->clt_fd, "501 Syntax error in parameters or arguments.\r\n");
  else
    {
      if (chan->data.mode != NONE)
	close(chan->data.sock);
      if ((ret = build_data_chan_pasv(&chan->data)) == -1)
	return (-1);
      else if (ret == 0)
	dprintf(chan->clt_fd, "421 Service not available,"
		"closing control connection.\r\n");
      else
	{
	  print_pasv(chan->clt_fd, &chan->data);
	  chan->data.mode = PASSIV;
	}
    }
  return (1);
}

int     cmd_Port(const t_cmd *cmd, t_channels *chan, t_path *path)
{
  int	ret;

  (void)path;
  if (strcasecmp(cmd->cmd, "PORT"))
    return (0);
  if (!cmd->param)
    dprintf(chan->clt_fd, "501 Syntax error in parameters or arguments.\r\n");
  else
    {
      if (chan->data.mode != NONE)
	close(chan->data.sock);
      if ((ret = build_data_chan_port(&chan->data, cmd->param)) == -1)
	return (-1);
      else if (ret == 0)
	dprintf(chan->clt_fd, "501 Syntax error in parameters"
		"or arguments.\r\n");
      else
	{
	  dprintf(chan->clt_fd, "200 Command okay.\r\n");
	  chan->data.mode = ACTIV;
	  chan->data.sock = -1;
	}
    }
  return (1);
}
