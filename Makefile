##
## Makefile for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
## 
## Made by Karmes Lionel
## Login   <karmes_l@epitech.net>
## 
## Started on  Wed May  4 13:27:22 2016 Karmes Lionel
## Last update Sun May 15 21:22:35 2016 Karmes Lionel
##

CC	= gcc

RM	= rm -f

SRCS	= myftp.c \
	path.c \
	checkArgs.c \
	readClient.c \
	authentification.c \
	parseCmd.c \
	exec_cmd.c \
	cmd_X.c \
	cmd_path.c \
	cmd_list.c \
	cmd_data.c \
	cmd_data_transfert.c \
	channels.c \
	channels_socket.c \
	my_signal.c

OBJS	= $(SRCS:.c=.o)

NAME	= server

CFLAGS	+= -Wall -Wextra -Werror
CFLAGS	+= -I./include

LDFLAGS	=


all:	$(NAME)

$(NAME): $(OBJS)
	 @printf "\033[032m --- COMPILING ---\033[0m\n"
	 $(CC) $(OBJS) -o $(NAME) $(LDFLAGS)

clean:
	@printf "\033[032m --- CLEAN ---\033[0m\n"
	$(RM) $(OBJS)

fclean: clean
	@printf "\033[032m --- FCLEAN ---\033[0m\n"
	$(RM) $(NAME)

re: fclean all
	@printf "\033[032m --- RE DONE---\033[0m\n"

.PHONY: all clean fclean re
