/*
** path.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Sun May  8 15:23:30 2016 Karmes Lionel
** Last update Sun May 15 21:17:19 2016 Karmes Lionel
*/

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include "path.h"

int	init_path(t_path *path, const char *real)
{
  if (chdir(real) == -1)
    return (fprintf(stderr, "Err: chdir('%s') %s\n",
		    real, strerror(errno)), 0);
  if (!getcwd(path->root_real, PATH_MAX))
    return (fprintf(stderr, "Err: getcwd() %s\n",
		    strerror(errno)), 0);
  if (path->root_real[strlen(path->root_real) - 1] != '/')
    strcat(path->root_real, "/");
  strcpy(path->real, path->root_real);
  path->rela = strrchr(path->real, '/');
  return (1);
}

int	valid_dir(const t_path *path, const char *rela, int f_mov_dir)
{
  char	tmp[PATH_MAX];
  int	ret;
  int	count;

  if (rela[0] == '/')
    strcpy(tmp, path->root_real);
  else
    strcpy(tmp, path->real);
  strcat(tmp, rela);
  printf("tmp-1 :%s\n", tmp);
  if (chdir(tmp) == -1)
    return (0);
  ret = 2;
  printf("tmp0 :%s\n", tmp);
  if (!getcwd(tmp, PATH_MAX))
    return (fprintf(stderr, "Err: getcwd() %s\n",
		    strerror(errno)), -1);
  else if (!(count = strncmp(tmp, path->root_real,
			     strlen(path->root_real) - 1)))
    ret = 1;
  printf("tmp1 :%s\n", tmp);
  if (!f_mov_dir && chdir(path->real) == -1)
    return (fprintf(stderr, "Err: chdir('%s') %s\n", tmp,
		    strerror(errno)), -1);
  return (ret);
}

int	valid_path(const t_path *path, const char *rela)
{
  char	tmp[PATH_MAX];
  char	*c;
  int	ret;

  strcpy(tmp, rela);
  if ((c = strrchr(tmp, '/')))
    *(c + 1) = '\0';
  else
    strcpy(tmp, ".");
  if ((ret = valid_dir(path, tmp, 0)) != 1)
    return (ret);
  if (rela[0] == '/')
    strcpy(tmp, path->root_real);
  else
    strcpy(tmp, path->real);
  strcat(tmp, rela);
  if (access(tmp, F_OK) == -1)
    return (0);
  return (1);
}
