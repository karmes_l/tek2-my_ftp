/*
** signal.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Reseau/PSU_2015_myftp
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Sun May 15 14:58:04 2016 Karmes Lionel
** Last update Sun May 15 21:22:25 2016 Karmes Lionel
*/

#include <sys/wait.h>
#include <stdlib.h>
#include "my_signal.h"

void	sig_child(int signum)
{
  (void)signum;
  wait(NULL);
}
